const exBankingCore = require('ex-banking-core')

const client = new exBankingCore.Client()

export const getAllUsers = () => client.User.getAll()

export const createUser = (username: string) => client.User.createUser(username)

export const getBalance = (username: string) => client.Account.getBalance(username)

export const depositFunds = (username: string, amount: number, currency: string) =>
    client.Account.deposit(username, amount, currency)

export const withdrawalFunds = (username: string, amount: number, currency: string) =>
    client.Account.withdraw(username, amount, currency)

export const sendFunds = (
    senderUsername: string,
    receiverUsername: string,
    amount: number,
    currency: string
) => client.Account.send(senderUsername, receiverUsername, amount, currency)