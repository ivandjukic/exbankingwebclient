import React, {useState} from 'react'
import {sendFunds} from "../lib/exbanking";

interface SendFundsModalProps {
    onClose: React.MouseEventHandler,
    senderUsername: string
    onMoneySent: () => void
}

const SendFundsModal = ({onClose, senderUsername, onMoneySent}: SendFundsModalProps) => {

    const [receiverUsername, setReceiverUsername] = useState('')
    const [amount, setAmount] = useState(0)
    const [currency, setCurrency] = useState('')
    const [error, setError] = useState(null)

    const sendFundsHandler = () => {
        setError(null)
        if(!receiverUsername.length) {
            setError('You need to specify receiver')
        } else if(!currency.length) {
            setError('You need to specify currency')
        } else if (amount <= 0) {
            setError('Amount must be greater than 0')
        } else {
            const response = sendFunds(senderUsername, receiverUsername, amount, currency)
            if (response.success) {
                onMoneySent()
            } else {
                setError(response.message)
            }
        }
    }

    return (
        <div className="fixed z-10 inset-0 overflow-y-auto" aria-labelledby="modal-title" role="dialog"
             aria-modal="true">
            <div className="flex items-end justify-center h-64 pt-4 px-4 pb-12 text-center sm:block sm:p-0">
                <div className="fixed inset-0 bg-gray-500 bg-opacity-75 transition-opacity" aria-hidden="true"></div>

                <span className="hidden sm:inline-block sm:align-middle sm:h-screen" aria-hidden="true">&#8203;</span>
                <div
                    className="inline-block align-bottom bg-white rounded-lg text-left overflow-hidden shadow-xl transform
                    transition-all sm:my-8 sm:align-middle sm:max-w-lg sm:w-full h-auto">
                    <div>
                        <div className="flex px-8 pt-4 flex items-center">
                            <div className="pr-8">
                                <label className="block text-center text-sm font-medium text-gray-700 px-0 py-4">
                                    Sender
                                </label>
                                <input disabled={true} value={senderUsername}/>
                            </div>
                            <div className="pl-8">
                                <label className="block text-sm text-center font-medium text-gray-700 px-0 py-4">Receiver</label>
                                <input type="text"
                                       value={receiverUsername}
                                       onChange={e => setReceiverUsername(e.target.value) }
                                       className="focus:ring-indigo-500 h-8 focus:border-indigo-500 block w-full pl-7 pr-12 sm:text-sm border-gray-300"
                                       placeholder="John Doe"/>
                            </div>
                        </div>
                        <div className="flex">
                            <div className="mt-1 relative rounded-md shadow-sm px-8">
                                <label className="block text-sm text-center font-medium text-gray-700 p-4">
                                    Amount
                                </label>
                                <div className="absolute inset-y-0 left-0 pl-3 flex items-center pointer-events-none">
                                </div>
                                <input type="text"
                                       value={amount}
                                       onChange={e =>
                                           setAmount(Number(e.target.value.replace(/[^\d.-]/g, '')))
                                       }
                                       className="focus:ring-indigo-500 h-8 focus:border-indigo-500 block w-full pl-7 pr-12 sm:text-sm border-gray-300"
                                       placeholder="John Doe"/>
                            </div>
                            <div className="mt-1 relative rounded-md shadow-sm px-8">
                                <label className="block text-sm text-center font-medium text-gray-700 p-4">
                                    Currency
                                </label>
                                <div className="absolute inset-y-0 left-0 pl-3 flex items-center pointer-events-none">
                                </div>
                                <input type="text"
                                       value={currency}
                                       onChange={e =>
                                           setCurrency(e.target.value.replace(/[0-9]/g, ''))
                                       }
                                       className="focus:ring-indigo-500 h-8 focus:border-indigo-500 block w-full pl-7 pr-12 sm:text-sm border-gray-300"
                                       placeholder="EUR"/>
                            </div>
                        </div>
                    </div>
                    <span className="text-red-400 px-8">{error}</span>
                    <div className="bg-gray-50 px-4 py-3 sm:px-6 sm:flex sm:flex-row-reverse mt-6">
                        <button type="button" onClick={sendFundsHandler}
                                className="w-full inline-flex justify-center rounded-md border border-transparent shadow-sm px-4 py-2 bg-green-800 text-base font-medium text-white hover:bg-red-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-red-500 sm:ml-3 sm:w-auto sm:text-sm">
                            Confirm
                        </button>
                        <button type="button" onClick={onClose}
                                className="mt-3 w-full inline-flex justify-center rounded-md border border-gray-300 shadow-sm px-4 py-2 bg-white text-base font-medium text-gray-700 hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500 sm:mt-0 sm:ml-3 sm:w-auto sm:text-sm">
                            Cancel
                        </button>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default SendFundsModal