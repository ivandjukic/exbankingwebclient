import React, {useEffect, useState} from 'react'
import UserTableSingleRow from "./UserTableSingleRow";
import AddUserButton from "./AddUserButton";
import AddUserModal from "./AddUserModal";
import {getAllUsers} from "../lib/exbanking";
import DepositModal from "./DepositModal";
import BalanceModal from "./BalanceModal";
import WithdrawalModal from "./WithdrawalModal";
import SendFundsModal from "./SendFundsModal";

const Users = () => {
    const [showAddUserModal, setShowAddUserModal] = useState(false)
    const [showDepositModal, setShowDepositModal] = useState(false)
    const [showWithdrawalModal, setShowWithdrawalModal] = useState(false)
    const [showBalanceModal, setShowBalanceModal] = useState(false)
    const [showSendMoneyModal, setShowSendMoneyModal] = useState(false)
    const [users, setUsers] = useState([])
    const [selectedUser, setSelectedUser] = useState(null)

    useEffect(() => {
        setUsers(getAllUsers())
    }, [])

    const toggleShowAddUserModal: React.MouseEventHandler = () => setShowAddUserModal(!showAddUserModal)
    return (
        <div className="overflow-x-auto w-full">
            <div
                className="min-w-screen min-h-screen bg-gray-100 flex items-center justify-center bg-gray-100 font-sans overflow-hidden">
                <div className="w-full lg:w-3/6">
                    <div className="bg-white shadow-md rounded my-6">
                        <AddUserButton toggleShowAddUserModal={toggleShowAddUserModal}/>
                        <AddUserModal show={showAddUserModal}
                                      onClose={() => setShowAddUserModal(false)}
                                      onUserCreated={() => setShowAddUserModal(false)}
                        />
                        { showDepositModal &&
                            <DepositModal
                                onClose={() => setShowDepositModal(false)}
                                onDepositPlaced={() => setShowDepositModal(false)}
                                username={selectedUser?.username ?? null}
                            />
                        }
                        { showWithdrawalModal &&
                            <WithdrawalModal
                                onClose={() => setShowWithdrawalModal(false)}
                                onWithdrawal={() => setShowWithdrawalModal(false)}
                                username={selectedUser?.username ?? null}
                            />
                        }
                        { showBalanceModal &&
                            <BalanceModal
                               onClose={() => setShowBalanceModal(false)}
                               username={selectedUser?.username ?? null}
                            />
                        }
                        { showSendMoneyModal &&
                        <SendFundsModal
                            onClose={() => setShowSendMoneyModal(false)}
                            senderUsername={selectedUser?.username ?? null}
                            onMoneySent={() => setShowSendMoneyModal(false)}
                        />
                        }
                        <table className="min-w-max w-full table-auto">
                            <thead>
                            <tr className="bg-gray-200 text-gray-600 uppercase text-sm leading-normal">
                                <th className="py-3 px-6 text-left">User</th>
                                <th className="py-3 px-6 text-center">Deposit</th>
                                <th className="py-3 px-6 text-center">Withdraw</th>
                                <th className="py-3 px-6 text-center">Get Balance</th>
                                <th className="py-3 px-6 text-center">Send</th>
                            </tr>
                            </thead>
                            <tbody className="text-gray-600 text-sm font-light">
                                {users.map(user => <UserTableSingleRow
                                    key={user.id}
                                    username={user.username}
                                    onShowDepositModalCLicked={() => {
                                        setSelectedUser(user)
                                        setShowDepositModal(true)
                                    }}
                                    onShowWithdrawalModalCLicked={() => {
                                        setSelectedUser(user)
                                        setShowWithdrawalModal(true)
                                    }}
                                    onShowBalanceModalClicked={() => {
                                        setSelectedUser(user)
                                        setShowBalanceModal(true)
                                    }}
                                    onSendMoneyModalClicked={() => {
                                        setSelectedUser(user)
                                        setShowSendMoneyModal(true)
                                    }}
                                />
                                )}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    )
}


export default Users