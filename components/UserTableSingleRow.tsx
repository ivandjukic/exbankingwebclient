import React from 'react'

interface UserTableSingleRow {
    username: string,
    onShowDepositModalCLicked: React.MouseEventHandler,
    onShowWithdrawalModalCLicked: React.MouseEventHandler,
    onShowBalanceModalClicked: React.MouseEventHandler,
    onSendMoneyModalClicked: React.MouseEventHandler,
}

const UserTableSingleRow = ({
    username,
    onShowDepositModalCLicked,
    onShowWithdrawalModalCLicked,
    onShowBalanceModalClicked,
    onSendMoneyModalClicked
}) => {
    return (
        <tr className="border-b border-gray-200 hover:bg-gray-100">
            <td className="py-3 px-6 text-left">
                <div className="flex items-center">
                    <div className="mr-2">
                        <img className="w-6 h-6 rounded-full"
                             src="https://randomuser.me/api/portraits/men/1.jpg"/>
                    </div>
                    <span>{username}</span>
                </div>
            </td>

            <td className="py-3 px-6 text-center" onClick={onShowDepositModalCLicked}>
                <div className="flex justify-center">
                    <svg xmlns="http://www.w3.org/2000/svg" className="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                        <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M4 16v1a3 3 0 003 3h10a3 3 0 003-3v-1m-4-4l-4 4m0 0l-4-4m4 4V4" />
                    </svg>
                </div>
            </td>

            <td className="py-3 px-6 text-center" onClick={onShowWithdrawalModalCLicked}>
                <div className="flex justify-center">
                    <svg xmlns="http://www.w3.org/2000/svg" className="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                        <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M4 16v1a3 3 0 003 3h10a3 3 0 003-3v-1m-4-8l-4-4m0 0L8 8m4-4v12" />
                    </svg>
                </div>
            </td>

            <td className="py-3 px-6 text-center" onClick={onShowBalanceModalClicked}>
                <div className="flex justify-center">
                    <svg xmlns="http://www.w3.org/2000/svg" className="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                        <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M15 12a3 3 0 11-6 0 3 3 0 016 0z" />
                        <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M2.458 12C3.732 7.943 7.523 5 12 5c4.478 0 8.268 2.943 9.542 7-1.274 4.057-5.064 7-9.542 7-4.477 0-8.268-2.943-9.542-7z" />
                    </svg>
                </div>
            </td>

            <td className="py-3 px-6 text-center" onClick={onSendMoneyModalClicked}>
                <div className="flex justify-center">
                    <svg xmlns="http://www.w3.org/2000/svg" className="h-5 w-5" viewBox="0 0 20 20" fill="currentColor">
                        <path d="M8 5a1 1 0 100 2h5.586l-1.293 1.293a1 1 0 001.414 1.414l3-3a1 1 0 000-1.414l-3-3a1 1 0 10-1.414 1.414L13.586 5H8zM12 15a1 1 0 100-2H6.414l1.293-1.293a1 1 0 10-1.414-1.414l-3 3a1 1 0 000 1.414l3 3a1 1 0 001.414-1.414L6.414 15H12z" />
                    </svg>
                </div>
            </td>

        </tr>
    )
}


export default UserTableSingleRow