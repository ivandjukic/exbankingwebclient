import React from 'react';

interface ToggleSHowUserModalButtonProps {
    toggleShowAddUserModal: React.MouseEventHandler
}

const ToggleSHowUserModalButton = ({toggleShowAddUserModal}: ToggleSHowUserModalButtonProps) => {
    return (
        <div className="mt-8 flex lg:mt-0 lg:flex-shrink-0" onClick={toggleShowAddUserModal}>
            <div className="inline-flex rounded-md shadow">
                <a href="#"
                   className="inline-flex items-center justify-center px-5 py-3 border border-transparent text-base font-medium rounded-md text-white bg-green-600 hover:bg-indigo-700">
                    Add User
                </a>
            </div>
        </div>
    )
}

export default ToggleSHowUserModalButton