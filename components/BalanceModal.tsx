import React, {useEffect, useState} from 'react'
import {getBalance} from "../lib/exbanking";
import UserTableSingleRow from "./UserTableSingleRow";

interface DepositModalProps {
    onClose: React.MouseEventHandler,
    username: string | null
}

const BalanceModal = ({onClose, username}: DepositModalProps) => {
    const [balance, setBalance] = useState([])

    useEffect(() => {
        if (username) {
            setBalance(getBalance(username))
            console.log(balance)
        }
    }, [username])

    return (
        <div className="fixed z-10 inset-0 overflow-y-auto" aria-labelledby="modal-title" role="dialog"
             aria-modal="true">
            <div className="flex items-end justify-center min-h-screen pt-4 px-4 pb-12 text-center sm:block sm:p-0">
                <div className="fixed inset-0 bg-gray-500 bg-opacity-75 transition-opacity" aria-hidden="true"></div>
                <span className="hidden sm:inline-block sm:align-middle sm:h-screen" aria-hidden="true">&#8203;</span>
                <div
                    className="inline-block align-bottom bg-white rounded-lg text-left overflow-hidden shadow-xl transform
                    transition-all sm:my-8 sm:align-middle sm:max-w-lg sm:w-full h-auto">
                   <div className="p-8">
                       <table className="w-full p-8 table-auto">
                           <thead>
                           <tr className="bg-gray-200 text-gray-600 uppercase text-sm leading-normal">
                               <th className="py-3 px-6 text-center">Currency</th>
                               <th className="py-3 px-6 text-center">Amount</th>
                           </tr>
                           </thead>
                           <tbody className="text-gray-600 text-sm font-light">
                           {balance.map(currency => {
                               return (
                                   <tr className="border-b border-gray-200 hover:bg-gray-100" key={currency.currencyName}>
                                       <td className="py-3 px-6 text-left">
                                           <div className="flex justify-center">
                                               <span>{currency.currencyName}</span>
                                           </div>
                                       </td>
                                       <td className="py-3 px-6 ">
                                           <div className="flex justify-center">
                                               <span>{currency.balance}</span>
                                           </div>
                                       </td>
                                   </tr>
                               )
                           })}
                           </tbody>
                       </table>
                   </div>
                    <div className="bg-gray-50 px-4 py-3 sm:px-6 sm:flex sm:flex-row-reverse mt-6">
                        <button type="button" onClick={onClose}
                                className="mt-3 w-full inline-flex justify-center rounded-md border border-gray-300 shadow-sm px-4 py-2 bg-white text-base font-medium text-gray-700 hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500 sm:mt-0 sm:ml-3 sm:w-auto sm:text-sm">
                            Close
                        </button>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default BalanceModal