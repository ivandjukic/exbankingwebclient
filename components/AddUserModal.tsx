import React, {useState} from 'react'
import {createUser} from "../lib/exbanking";

interface AddUserModalProps {
    show: boolean,
    onClose: React.MouseEventHandler,
    onUserCreated: () => void
}

const AddUserModal = ({show, onClose, onUserCreated}: AddUserModalProps) => {

    const [username, setUsername] = useState('')
    const [error, setError] = useState(null)

    const saveUser = () => {
        setError(false)
        if (!username.length) {
            setError('Username is empty')
        } else {
            const response = createUser(username)
            if (response.success) {
                onUserCreated()
            } else {
                setError(response.message)
            }
        }
    }

    if (!show) return null
    return (
        <div className="fixed z-10 inset-0 overflow-y-auto" aria-labelledby="modal-title" role="dialog"
             aria-modal="true">
            <div className="flex items-end justify-center min-h-screen pt-4 px-4 pb-12 text-center sm:block sm:p-0">
                <div className="fixed inset-0 bg-gray-500 bg-opacity-75 transition-opacity" aria-hidden="true"></div>
                <span className="hidden sm:inline-block sm:align-middle sm:h-screen" aria-hidden="true">&#8203;</span>
                <div
                    className="inline-block align-bottom bg-white rounded-lg text-left overflow-hidden shadow-xl transform
                    transition-all sm:my-8 sm:align-middle sm:max-w-lg sm:w-full h-48">
                    <div>
                        <label htmlFor="price" className="block text-sm font-medium text-gray-700 p-4">Username</label>
                        <div className="mt-1 relative rounded-md shadow-sm px-8">
                            <div className="absolute inset-y-0 left-0 pl-3 flex items-center pointer-events-none">
                            </div>
                            <input type="text" name="price" id="price"
                                   onChange={e => setUsername(e.target.value)}
                                   className="focus:ring-indigo-500 h-8 focus:border-indigo-500 block w-full pl-7 pr-12 sm:text-sm border-gray-300"
                                   placeholder="John Doe"/>
                        </div>
                    </div>
                    <span className="text-red-400 px-8">{error}</span>
                    <div className="bg-gray-50 px-4 py-3 sm:px-6 sm:flex sm:flex-row-reverse mt-6">
                        <button type="button"  onClick={saveUser} className="w-full inline-flex justify-center rounded-md border border-transparent shadow-sm px-4 py-2 bg-green-800 text-base font-medium text-white hover:bg-red-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-red-500 sm:ml-3 sm:w-auto sm:text-sm">
                            Save
                        </button>
                        <button type="button" onClick={onClose} className="mt-3 w-full inline-flex justify-center rounded-md border border-gray-300 shadow-sm px-4 py-2 bg-white text-base font-medium text-gray-700 hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500 sm:mt-0 sm:ml-3 sm:w-auto sm:text-sm">
                            Cancel
                        </button>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default AddUserModal