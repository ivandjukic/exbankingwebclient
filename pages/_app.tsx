import Head from 'next/head'
import 'tailwindcss/tailwind.css'

import '../styles/global.css'

function Home({ Component, pageProps }) {
    return (
        <div>
            <Head>
                <title>Ex Banking Client</title>
            </Head>
            <div className="app bg-cover h-full">
                <main>
                    <Component {...pageProps} />
                </main>
            </div>
        </div>
    )
}

export default Home
